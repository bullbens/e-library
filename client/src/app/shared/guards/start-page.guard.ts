import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class StartPageGuard implements CanActivate {

  constructor() { }

  canActivate(): boolean {
    const token = localStorage.getItem('token');
    if (token) {
      return false;
    }
    return true;
  }
}
