import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor () {
  }
  public role = JSON.parse(localStorage.getItem('role'));
  canActivate(): boolean {
    return true;
  }
}
