import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IMagazineModel } from '../../../../../shared/models/magazine.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class MagazineConnectionServer {
  public token = localStorage.getItem('token');
  public auth: string;
  private url = environment.url;
  constructor(private http: HttpClient) { }
  
  public getMagazine() {
    return this.http.get(this.url + '/magazine/', { headers: { 'x-access-token': this.token } });
  }

  public getMagazineById(_id: string) {
    return this.http.get(this.url + '/magazine/' + _id + '/', { headers: { 'x-access-token': this.token } });
  }

  public addMagazine(magazine: IMagazineModel) {
    return this.http.post(this.url + '/magazine/', magazine, { headers: { 'x-access-token': this.token } });
  }
  public updateMagazineById(magazine: IMagazineModel ,_id: string) {
    return this.http.put(this.url + '/magazine/' + _id + '/', magazine , { headers: { 'x-access-token': this.token } });
  }
  public deleteMagazineById( _id: string) {
    return this.http.delete(this.url + '/magazine/' + _id + '/', { headers: { 'x-access-token': this.token } });
  }
}
