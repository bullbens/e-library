import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { IBookModel } from '../../../../../shared/models/book.model';

@Injectable({
    providedIn: 'root'
})

export class BookConnectionServer {
    public token = localStorage.getItem('token');
    public auth: string;
    private url = environment.url;
    constructor(private http: HttpClient) { }
    // [showPageBook, numberOfBooks] ,\
    // showPageBook: number, numberOfBooks: number
    public getBook() {
        return this.http.get(this.url + '/book/', { headers: { 'x-access-token': this.token } });
    }
    public getAuthorsBookById(bookId: string) {
        return this.http.get(this.url + '/book/authors/' + bookId + '/', { headers: { 'x-access-token': this.token } });
    }
    public getBookById(bookId: string) {
        return this.http.get(this.url + '/book/' + bookId + '/', { headers: { 'x-access-token': this.token } });
    }
    public getBookWithoutAuthor() {
        return this.http.get(this.url + '/books/without-author/', { headers: { 'x-access-token': this.token } });
    }
    public getBookBySerach(title: string) {
        return this.http.get(this.url + "/book/search/"+ title + "/", { headers: { 'x-access-token': this.token } });
    }

    public addBook(book: IBookModel) {
        return this.http.post<IBookModel>(this.url + '/book/', book, { headers: { 'x-access-token': this.token } });
    }

    public deleteBook(bookId: string) {
        return this.http.delete(this.url + '/book/' + bookId + '/', { headers: { 'x-access-token': this.token } });
    }

    public updateBookById(book: IBookModel, _id: any) {
        return this.http.put(this.url + '/book/' + _id + '/', book, { headers: { 'x-access-token': this.token } });
    }

}
