import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { IUserModel } from '../../../../../shared/models/user.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AdminConnectionServer {
  public token = localStorage.getItem('token');
  public auth: string;
  private url = environment.url;
  constructor(private http: HttpClient) { }

  public getUsers() {
    return this.http.get(this.url + '/admin/users', { headers: { 'x-access-token': this.token } });
  }

  public getUserById(userId: string) {
    return this.http.get(this.url + '/admin/user/' + userId + '/', { headers: { 'x-access-token': this.token } });
  }

  public getClosedUsers() {
    return this.http.get(this.url + '/admin/closed', { headers: { 'x-access-token': this.token } });
  }

  public addUser(user: IUserModel): Observable<IUserModel> {
    return this.http.post<IUserModel>(this.url + '/auth/register', user);
  }

  public deleteUserById(userId: any) {
    return this.http.delete(this.url + '/admin/user/' + userId + '/', { headers: { 'x-access-token': this.token } });
  }

  public updateUserById(user: IUserModel, _id: any) {
    return this.http.put(this.url + '/user/update/' + _id + '/', user, { headers: { 'x-access-token': this.token } });
  }
}
