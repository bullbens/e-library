import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUserModel } from '../../../../../shared/models/user.model';
import { Observable } from 'rxjs';
import { ILoginModel } from '../../../../../shared/models/login.model';

@Injectable({
  providedIn: 'root'
})

export class AuthConnectionServer {
  public token = localStorage.getItem('token');
  public auth: string;
  private url = environment.url;
  constructor(private http: HttpClient) { }

  public getUserRole() {
    return this.http.get(this.url + '/user/role', { headers: { 'x-access-token': this.token } });
  }
  public addUser(user: IUserModel): Observable<IUserModel> {
    return this.http.post<IUserModel>(this.url + '/auth/register', user);
  }
  public loginUser(user: ILoginModel) {
    return this.http.post<ILoginModel>(this.url + '/auth/login', user);
  }
}
