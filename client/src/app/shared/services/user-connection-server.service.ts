import { environment } from 'src/environments/environment';
import { IUserModel } from '../../../../../shared/models/user.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class UserConnectionServer {
  public token = localStorage.getItem('token');
  public auth: string;
  private url = environment.url;
  constructor(private http: HttpClient) { }

  public getProfileUser() {
    return this.http.get(this.url + '/auth/profile', { headers: { 'x-access-token': this.token } });
  }

  public getUserRole() {
    return this.http.get(this.url + '/user/role', { headers: { 'x-access-token': this.token } });
  }
  
  public updateUserById(user: IUserModel, _id: any) {
    console.log(user);
    return this.http.put(this.url + '/user/update/' + _id + '/', user, { headers: { 'x-access-token': this.token } });
  }
}
