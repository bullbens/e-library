import { Injectable } from '@angular/core';
import { AuthConnectionServer } from './auth-connection-server.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public role: number;

  constructor(private authConnectionServer: AuthConnectionServer) {}

  public token = JSON.stringify(localStorage.getItem('token'));
  public roleUser = JSON.parse(localStorage.getItem('role'));
  public closed: boolean;
}
