import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { IAuthorModel } from '../../../../../shared/models/author.model';

@Injectable({
  providedIn: 'root'
})

export class AuthorConnectionServer {
  public token = localStorage.getItem('token');
  public auth: string;
  private url = environment.url;
  constructor(private http: HttpClient) { }

  public getAuthor() {
    return this.http.get(this.url + '/author', { headers: { 'x-access-token': this.token } });
  }

  public getAuthorById(_id: string) {
    return this.http.get(this.url + '/author/' + _id + '/', { headers: { 'x-access-token': this.token } });
  }

  public getMagazinesByAuthorId(authorId: any) {
    return this.http.get(this.url + '/author/amgazine/' + authorId + '/', { headers: { 'x-access-token': this.token } });
  }

  public getBooksByAuthorId(authorId: any) {
    return this.http.get(this.url + '/author/book/' + authorId + '/', { headers: { 'x-access-token': this.token } });
  }

  public addAuthor(author: IAuthorModel) {
    return this.http.post<IAuthorModel>(this.url + '/author', author, { headers: { 'x-access-token': this.token } });
  }

  public updateAuthorById(author: IAuthorModel, _id: any) {
    return this.http.put(this.url + '/author/' + _id + '/', author, { headers: { 'x-access-token': this.token } });
  }

  public deleteAuthor(authorId: any) {
    return this.http.delete(this.url + '/author/' + authorId, { headers: { 'x-access-token': this.token } });
  }

}
