import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConteinerAdminHeaderComponent } from '../../admin/conteiner-admin-header/conteiner-admin-header.component';

@NgModule({
  declarations: [ConteinerAdminHeaderComponent],
  imports: [
    CommonModule
  ]
})
export class ConteinerUserHeaderModule { }
