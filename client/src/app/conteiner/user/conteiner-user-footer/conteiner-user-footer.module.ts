import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConteinerUserHeaderComponent } from '../conteiner-user-header/conteiner-user-header.component';

@NgModule({
  declarations: [ConteinerUserHeaderComponent],
  imports: [
    CommonModule
  ]
})
export class ConteinerUserFooterModule { }
