import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [ConteinerAdminFooterModule],
  imports: [
    CommonModule
  ]
})
export class ConteinerAdminFooterModule { }
