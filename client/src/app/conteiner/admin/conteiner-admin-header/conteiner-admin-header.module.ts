import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConteinerAdminHeaderComponent } from './conteiner-admin-header.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ConteinerAdminHeaderComponent],
  imports: [
    FormsModule,
    CommonModule
  ]
})
export class ConteinerAdminHeaderModule { }
