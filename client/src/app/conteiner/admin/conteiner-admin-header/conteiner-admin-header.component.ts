import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-conteiner-admin-header',
  templateUrl: './conteiner-admin-header.component.html',
  styleUrls: ['./conteiner-admin-header.component.scss']
})
export class ConteinerAdminHeaderComponent implements OnInit {

  public loginAdmin = this.cookie.get('login');

  constructor(private cookie: CookieService) { }

  ngOnInit() {
  }


  exitUsers() {
    localStorage.clear();
    this.cookie.deleteAll();
    return
  }

}
