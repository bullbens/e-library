import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserConnectionServer } from './shared/services/user-connection-server.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'FullStackThree';
  public envName = environment.name;

  constructor(private userConnectionServer: UserConnectionServer) { }

  public UserRole() {
    if (localStorage.getItem('token') == null) {
      return;
    }
    this.userConnectionServer.getUserRole().subscribe(data => {
      console.log(data);
      localStorage.setItem('role', data['role']);
    });
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    this.UserRole();
  }
}
