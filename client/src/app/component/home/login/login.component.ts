import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ILoginModel } from '../../../../../../shared/models/login.model';
import { CookieService } from 'ngx-cookie-service';
import { AuthConnectionServer } from 'src/app/shared/services/auth-connection-server.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  public isLoginUsers = false;
  public isLoginAdmin = false;
  public statusMessage: any;
  public message = '';
  public login = '';
  public password = '';
  public status = '';
  public closed: boolean;
  public rout = '';
  
  constructor(
    private cookie: CookieService,
    private authConnectionServer: AuthConnectionServer
    ) { }
    
    ngOnInit() {
    }
    
  public onSubmitSignIn(first: NgForm) {

    this.login = first.value.login;
    this.password = first.value.password;
    let editedUser: ILoginModel = {
      login: this.login,
      password: this.password
    };
    this.authConnectionServer.loginUser(editedUser).subscribe(data => {
      localStorage.setItem('token', data['token']);
      localStorage.setItem('role', data['user']['role']);
      let userData = (data['user']);
      this.closed = userData['closed'];
      if (this.closed === true) {
        this.message = 'На данный момент ваш аккаунт не действительный';
      }
      if (this.closed === false) {
        this.authConnectionServer.getUserRole().subscribe(() => {
          this.login = userData['login'];
        });
        this.cookie.set('login', this.login);
      }
    });
  }
}
export default LoginComponent;
