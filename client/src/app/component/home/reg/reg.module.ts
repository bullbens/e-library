import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegComponent } from './reg.component';

import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

const regRoutes: Routes = [
  {path: '', component: RegComponent}
]

@NgModule({
  declarations: [RegComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(regRoutes)
  ]
})
export class RegModule { }
