import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { IUserModel } from '../../../../../../shared/models/user.model';
import { AuthConnectionServer } from 'src/app/shared/services/auth-connection-server.service';

@Component({
  selector: 'app-reg',
  templateUrl: './reg.component.html',
  styleUrls: ['./reg.component.scss'],
  providers: []
})
export class RegComponent implements OnInit {

  public users: Array<IUserModel>;
  public isNewUsers: boolean;
  public statusMessage: string;
  public localStorageWrite = false;
  public newUser: boolean;
  public rout = '';

  constructor(private router: Router, private authConnectionServer: AuthConnectionServer) {
    this.users = new Array<IUserModel>();
  }

  ngOnInit() {

  }

  public onSubmitCheckIn(last: NgForm) {
    let userName = last.value.name;
    let userSurname = last.value.surname;
    let userEmail = last.value.email;
    let userLogin = last.value.login;
    let userPassword = last.value.password;
    let editedUser: IUserModel = {
      _id: '',
      name: userName,
      surname: userSurname,
      email: userEmail,
      login: userLogin,
      password: userPassword,
      status: 'user',
      closed: true,
      role: 0
    };
    this.users.push(editedUser);
    this.authConnectionServer.addUser(editedUser).subscribe(data => {
      this.isNewUsers = true;
      this.rout = '/login';
      this.router.navigate([this.rout]);
    });
  }
}
