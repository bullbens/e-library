import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import { HomeComponent } from './home.component';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './login/login.component';
import { RegComponent } from './reg/reg.component';

const homeRoutes: Routes = [
  {path: '', component: LoginComponent},
  {path: '', component: RegComponent}
]

@NgModule({
  declarations: [HomeComponent],
  imports: [
    NgModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(homeRoutes)
  ]
})
export class HomeModule { }
