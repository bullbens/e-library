import { Component, OnInit, Input } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { IUserModel } from '../../../../../../shared/models';

@Component({
  selector: 'app-admin-setting',
  templateUrl: './admin-setting.component.html',
  styleUrls: ['./admin-setting.component.scss']
})
export class AdminSettingComponent implements OnInit {
  @Input() kollClosedUsers: any;
  public dataUsers = this.cookie.get('login');
  public newUserBlock: boolean;
  public allUser: Array<IUserModel>;
  public newUserAdd: boolean = false;
  constructor(private cookie: CookieService) {
  }

  ngOnInit() {
  }

  public showNewUserBlock(): void {
    this.newUserBlock = !this.newUserBlock;
  }

  public closeFromNewUser(): void {
    this.newUserAdd = false;
  }
}
export default AdminSettingComponent;
