import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AllUsersComponent } from './all-users.component';
import { RouterModule, Routes } from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';
import { PopupAdminAddUserComponent } from 'src/app/component/popup/admin/user/popup-admin-add-user/popup-admin-add-user.component';
import { PopupAdminUpdateUserComponent } from 'src/app/component/popup/admin/user/popup-admin-update-user/popup-admin-update-user.component';

const userSettingRoutes: Routes = [
  {path: '', component: AllUsersComponent},
]

@NgModule({
  declarations: [AllUsersComponent, PopupAdminAddUserComponent, PopupAdminUpdateUserComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgxPaginationModule,
    RouterModule.forChild(userSettingRoutes)
  ]
})
export class AllUsersModule { }
