import { Component, OnInit, ElementRef, ViewChild, Input, HostListener, Output } from '@angular/core';
import { IUserModel } from '../../../../../../../shared/models/user.model';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { AdminConnectionServer } from 'src/app/shared/services/admin-connection-server.service';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})

export class AllUsersComponent implements OnInit {
  @ViewChild('btnRemove') btnRemome: ElementRef;
  @ViewChild('btnDelete') btnDelete: ElementRef;
  @Input() updateUserResult: boolean;
  public myForm: FormGroup;
  public liveSearch: Array<IUserModel>;
  public startAt = new Subject();
  public endAt = new Subject();
  public p: number = 1;
  public users: Array<IUserModel>;
  public searchUser: boolean = false;
  public allShowUser: boolean = true;
  public addUserBlock: boolean = false;
  public textAddUserButton: string = 'AddUser';
  public removeUserBlock: boolean = false;

  public user: Array<IUserModel>;

  constructor(private adminConnectionServer: AdminConnectionServer) { }

  public getUsers(): void {
    this.adminConnectionServer.getUsers().subscribe((data: Array<IUserModel>) => {
      this.users = data;
    });
  }

  public ngOnInit(): void {
    this.getUsers();
  }

  public addUserBlockFunction(event): void {
    this.addUserBlock = event;
    this.getUsers();
  }
  public removeUserFunction(event) {
    this.removeUserBlock = event;
    this.getUsers();
  }

  addUserBlockF(e): void {
    this.addUserBlock = e;
    this.getUsers();
  }

  public deleteUser(event): void {
    let id = this.users[event.target.value]._id;
    this.adminConnectionServer.deleteUserById(id).subscribe(data => {
      this.getUsers();
    });
  }

  public removeUser(user): void {
    this.user = user;
    this.removeUserBlock = !this.removeUserBlock;
  }

  public showFormAdd(): void {
    this.addUserBlock = !this.addUserBlock;
    if (this.addUserBlock) {
      this.textAddUserButton = 'Close';
    }
    if (this.addUserBlock === false) {
      this.textAddUserButton = 'Add user';
    }
  }
  public onKey(event): void {
    const q = event.target.value;
    this.liveSearch = [];
    this.liveSearch = this.users.filter(item => {
      if (item.name.toLowerCase().search(q.toLowerCase()) >= 0) {
        this.allShowUser = false;
        this.searchUser = true;
        return true;
      }
    });
    if (this.liveSearch.length <= 0) {
      this.liveSearch = this.users;
    }
    if (q === 0) {
      this.allShowUser = true;
      this.searchUser = false;
      this.liveSearch = [];
    }
  }
}
