import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminSettingComponent } from './admin-setting.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SettingLibraryOpenBookComponent } from './setting-library/setting-library-open-book/setting-library-open-book.component';

const adminSettingRoutes: Routes = [
  {
    path: '',
    component: AdminSettingComponent,
  },
  { path: 'users', loadChildren: '../admin-setting/all-users/all-users.module#AllUsersModule' },
  { path: 'closed', loadChildren: '../admin-setting/closed-users/closed-users.module#ClosedUsersModule' },
  { path: 'library', loadChildren: '../admin-setting/setting-library/setting-library.module#SettingLibraryModule' },

]

@NgModule({
  declarations: [
    AdminSettingComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild(adminSettingRoutes)
  ]
})
export class AdminSettingModule { }
