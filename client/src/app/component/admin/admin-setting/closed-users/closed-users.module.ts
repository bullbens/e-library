import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import ClosedUsersComponent from './closed-users.component';
import { Routes, RouterModule } from '@angular/router';

const closedSettingRoutes: Routes = [
  { path: '', component: ClosedUsersComponent },
]

@NgModule({
  declarations: [ClosedUsersComponent],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild(closedSettingRoutes)
  ]
})
export class ClosedUsersModule { }
