import { Component, OnInit } from '@angular/core';
import { IUserModel } from '../../../../../../../shared/models/user.model';
import { AdminConnectionServer } from 'src/app/shared/services/admin-connection-server.service';

@Component({
  selector: 'app-closed-users',
  templateUrl: './closed-users.component.html',
  styleUrls: ['./closed-users.component.scss']
})
export class ClosedUsersComponent implements OnInit {
  constructor(private adminConnectionServer: AdminConnectionServer) { }
  public user: Array<IUserModel>;
  public numberOfClosedUsers: number;
  public closedUsers: Array<IUserModel>;
  public closed: boolean = false;

  public getClosedUsers(): boolean {
    this.adminConnectionServer.getClosedUsers().subscribe((data: Array<IUserModel>) => {
      this.closedUsers = data;
      this.numberOfClosedUsers = this.closedUsers.length;
    });
    return true;
  }
  public updateUserById(editedUser, _id): boolean {
    this.adminConnectionServer.updateUserById(editedUser, _id).subscribe(data => {
      this.getClosedUsers();
    });
    return true;
  }

  ngOnInit() {
    this.getClosedUsers();
  }

  public openUser(event): void {
    const id = event.target.value;
    this.adminConnectionServer.getUserById(id).subscribe(data => {
      const editedUser: IUserModel = {
        _id: id,
        name: data['name'],
        surname: data['surname'],
        email: data['email'],
        login: data['login'],
        password: data['password'],
        status: data['status'],
        closed: false,
        role: 1
      };
      this.updateUserById(editedUser, id);
    });
  }
  public openAdmin(event): void {
    const id = event.target.value;
    this.adminConnectionServer.getUserById(id).subscribe(data => {
      const editedUser: IUserModel = {
        _id: id,
        name: data['name'],
        surname: data['surname'],
        email: data['email'],
        login: data['login'],
        password: data['password'],
        status: 'admin',
        closed: false,
        role: 2
      };
      this.updateUserById(editedUser, id);
    });
  }

  public deleteUser(event): void {
    const id = event.target.value;
    this.adminConnectionServer.deleteUserById(id).subscribe(data => {
      console.log(data);
      this.getClosedUsers();
    });
  }
}
export default ClosedUsersComponent;

