import { Component, OnInit } from '@angular/core';
import { BookConnectionServer } from 'src/app/shared/services/book-connection-server.service';
import { IBookModel } from '../../../../../../../../shared/models';

@Component({
  selector: 'app-without-an-authors',
  templateUrl: './without-an-authors.component.html',
  styleUrls: ['./without-an-authors.component.scss']
})
export class WithoutAnAuthorsComponent implements OnInit {

  public bookWithoutAuthor;


  constructor(private bookConnectionServer: BookConnectionServer) {}

  ngOnInit() {
      this.bookConnectionServer.getBookWithoutAuthor().subscribe(data => {
        console.log(data);
        this.bookWithoutAuthor = data;
      });
  }
}
