import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WithoutAnAuthorsComponent } from './without-an-authors.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

const withoutAnAuthorsRoutes: Routes = [
  {
    path: '', 
    component: WithoutAnAuthorsComponent,
  }
]

@NgModule({
  declarations: [WithoutAnAuthorsComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(withoutAnAuthorsRoutes)
  ]
})
export class WithoutAnAuthorsModule { }
