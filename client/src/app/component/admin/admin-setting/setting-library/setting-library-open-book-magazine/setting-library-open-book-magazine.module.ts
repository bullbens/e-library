import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingLibraryOpenBookMagazineComponent } from './setting-library-open-book-magazine.component';
import { Routes, RouterModule } from '@angular/router';

const settingLibraryOpenBookMagazineRoutes: Routes = [
  {
    path: '', 
    component: SettingLibraryOpenBookMagazineComponent,
  }
]

@NgModule({
  declarations: [SettingLibraryOpenBookMagazineComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(settingLibraryOpenBookMagazineRoutes)
  ]
})
export class SettingLibraryOpenBookMagazineModule { }
