import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { IMagazineModel } from '../../../../../../../../shared/models';
import { MagazineConnectionServer } from 'src/app/shared/services/magazine-connection-server.service';

@Component({
  selector: 'app-setting-library-open-book-magazine',
  templateUrl: './setting-library-open-book-magazine.component.html',
  styleUrls: ['./setting-library-open-book-magazine.component.scss']
})
export class SettingLibraryOpenBookMagazineComponent implements OnInit {

  public _id: string;
  public magazineData: Array<IMagazineModel>;
  public subscription: Subscription;

  constructor(private activateRoute: ActivatedRoute,private magazineConnectionServer: MagazineConnectionServer) {
    this.subscription = activateRoute.params.subscribe(params => {
      this._id = params['id'];
    });
   }

  ngOnInit() {
    this.magazineConnectionServer.getMagazineById(this._id).subscribe((data: Array<IMagazineModel>) => {
      this.magazineData = data;
    });
  }
}
