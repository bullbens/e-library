import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { IBookModel } from '../../../../../../../../shared/models/book.model';
import { BookConnectionServer } from 'src/app/shared/services/book-connection-server.service';
import { IAuthorModel } from '../../../../../../../../shared/models/author.model';
import { AuthorConnectionServer } from 'src/app/shared/services/author-connection-server.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'setting-library-open-book',
  templateUrl: './setting-library-open-book.component.html',
  styleUrls: ['./setting-library-open-book.component.scss']
})
export class SettingLibraryOpenBookComponent implements OnInit {

  public id: string;
  public bookData: Array<IBookModel>;
  public authorData: Array<IAuthorModel>;
  public authorId = [];
  private subscription: Subscription;
  // public getAuthor(): void {
  //   this.bookConnectionServer.getAuthorsBookById(this.id).subscribe((data: Array<IAuthorModel>) => {
  //     this.authorData = data;
  //     console.log(this.authorData);
  //   });
  // }

  constructor(private activateRoute: ActivatedRoute, private bookConnectionServer: BookConnectionServer, private authorConnectionServer: AuthorConnectionServer) {
    this.subscription = activateRoute.params.subscribe(params => {
      this.id = params['id'];
    });
  }
  ngOnInit() {
    this.bookConnectionServer.getBookById(this.id).subscribe((data: Array<IBookModel>) => {
      this.bookData = data;
      console.log(this.bookData);
      this.authorId = this.bookData['authorId'];
    });
  }
}
