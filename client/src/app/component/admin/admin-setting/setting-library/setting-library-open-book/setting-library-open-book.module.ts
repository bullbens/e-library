import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingLibraryOpenBookComponent } from './setting-library-open-book.component';
import { Routes, RouterModule } from '@angular/router';

const settingLibraryOpenBookRoutes: Routes = [
  {
    path: '', 
    component: SettingLibraryOpenBookComponent,
  }
]

@NgModule({
  declarations: [SettingLibraryOpenBookComponent],
  imports: [
    CommonModule,
   RouterModule.forChild(settingLibraryOpenBookRoutes),
  ]
})
export class SettingLibraryOpenBookModule { }
