import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAuthorModel } from '../../../../../../../shared/models/author.model';
import { IBookModel } from '../../../../../../../shared/models/book.model';
import { Router } from '@angular/router';
import { IMagazineModel } from '../../../../../../../shared/models/magazine.model';
import { BookConnectionServer } from 'src/app/shared/services/book-connection-server.service';
import { AuthorConnectionServer } from 'src/app/shared/services/author-connection-server.service';
import { MagazineConnectionServer } from 'src/app/shared/services/magazine-connection-server.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-setting-library',
  templateUrl: './setting-library.component.html',
  styleUrls: ['./setting-library.component.scss']
})
export class SettingLibraryComponent implements OnInit {

  constructor(
    private bookConnectionServer: BookConnectionServer,
    private authorConnectionServer: AuthorConnectionServer,
    private magazineConnectionServer: MagazineConnectionServer,
  ) {
  }
  public pa = 1;
  public _id: string;
  public liveSearchAuth: Array<IBookModel>;
  public _idAuthor: string;


  public magazines: Array<IMagazineModel>;

  public allMagazine: Array<IMagazineModel>;

  public addMagazineBlock: boolean = false;
  public blockLibrary: boolean = true;
  public authorLibrary: boolean = false;
  public removeAuthorBlock: boolean = false;
  public authId: string;
  public openBlock: string = 'Update Authors';


  public showPageBook = 1;
  public numberOfBooks = 6;

  public allBooks: Array<IBookModel>;
  public allAuthors: Array<IAuthorModel>;

  public popupBook: boolean = false;
  public popupAuthor: boolean = false;
  public popupMagazine: boolean = false;

  public popupNameBook: string;
  public popupNameAuthor: string;
  public popupNameMagazine: string;

  public getBookData: Array<IBookModel>;
  public getAuthorData: Array<IAuthorModel>;
  public getMagazineData: Array<IMagazineModel>;

  public messageError: string;

  public getAuthor(): void {
    this.authorConnectionServer.getAuthor().subscribe((data: Array<IAuthorModel>) => {
      this.allAuthors = data;
    });
  }

  public getBook(): void {
    this.bookConnectionServer.getBook().subscribe((data: Array<IBookModel>) => {
      this.allBooks = data;
      this.getAuthor();
      console.log(this.allBooks);
    });
  }

  public getMagazine(): void {
    this.magazineConnectionServer.getMagazine().subscribe((data: Array<IMagazineModel>) => {
      this.allMagazine = data;
      this.getAuthor();
    });
  }
  ngOnInit() {
    this.getAuthor();
    this.getBook();
    this.getMagazine();
  }

  public showBlock(): void {
    this.blockLibrary = !this.blockLibrary;
    this.authorLibrary = !this.authorLibrary;
    this.getAuthor();
    this.getBook(); 
    if (this.authorLibrary === false) {
      this.openBlock = 'Update Authors';
    }
    if (this.authorLibrary === true) {
      this.openBlock = 'Update Library';
    }
  }

  public removeBook(getBookData): void {
    this.popupNameBook = "Remove Book";
    this.popupBook = !this.popupBook;
    this.getBookData = getBookData;
  }

  public deleteBook(event): void {
    let id = this.allBooks[event.target.value]._id;
    this.bookConnectionServer.deleteBook(id).subscribe(data => {
      this.getBook();
    });
  }

  public setBooksForm(): void {
    this.popupNameBook = "Add Book"
    this.popupBook = !this.popupBook;
  }

  public removeAuthor(getAuthorData): void {
    this.popupNameAuthor = "Remove Author";
    this.popupAuthor = !this.popupAuthor;
    this.getAuthorData = getAuthorData;
  }

  public deleteAuthor(event): void {
    let id = this.allAuthors[event.target.value]._id;
    this.authorConnectionServer.deleteAuthor(id).subscribe(data => {
      this.getAuthor();
    });
  }

  public setAuthorForm(): void {
    this.popupNameAuthor = "Add Author"
    this.popupAuthor = !this.popupAuthor;
  }

  public removeMagazine(getMagazineData): void {
    this.popupNameMagazine = "Remove Magazine";
    this.popupMagazine = !this.popupMagazine;
    this.getMagazineData = getMagazineData;
  }

  public deleteMagazine(event): void {
    let id = event.target.value;
    this.magazineConnectionServer.deleteMagazineById(id).subscribe(data => {
      this.getMagazine();
    });
  }

  public setMagazineForm(): void {
    this.popupNameMagazine = "Add Magazine";
    this.popupMagazine = !this.popupMagazine;
  }

  public popupBookResult(event): void {
    this.popupBook = event;
    this.getBook();
  }

  public popupAuthorResult(event): void {
    this.popupAuthor = event;
    this.getAuthor();
  }

  public popupMagazineResult(event): void {
    this.popupMagazine = event;
    this.getMagazine();
  }

  public onKey(event): void {
    let searchBook = event.target.value;
    if (this.blockLibrary) {
      if (searchBook === '') {
        this.bookConnectionServer.getBook().subscribe((data: Array<IBookModel>) => {
          this.allBooks = data;
        })
      }
      if (searchBook !== '') {
        this.bookConnectionServer.getBookBySerach(searchBook).subscribe((data: Array<IBookModel>) => {
          if (this.allBooks == []) {
            this.messageError = 'Book not found';
          }
          this.messageError = '';
          this.allBooks = data;
          console.log(this.allBooks);
        });
      }
    }
  }
}
