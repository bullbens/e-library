import { Component, OnInit } from '@angular/core';
import { IAuthorModel } from '../../../../../../../../shared/models/author.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { IBookModel } from '../../../../../../../../shared/models/book.model';
import { IMagazineModel } from '../../../../../../../../shared/models/magazine.model';
import { AuthorConnectionServer } from 'src/app/shared/services/author-connection-server.service';

@Component({
  selector: 'app-setting-library-open-author',
  templateUrl: './setting-library-open-author.component.html',
  styleUrls: ['./setting-library-open-author.component.scss']
})
export class SettingLibraryOpenAuthorComponent implements OnInit {

  public id: string;
  public AuthorData: Array<IAuthorModel>;
  public booksData: Array<IBookModel>;
  public magazinesData: Array<IMagazineModel>;
  private subscription: Subscription;

  constructor(private activateRoute: ActivatedRoute, private authorConnectionServer: AuthorConnectionServer) {
    this.subscription = activateRoute.params.subscribe(params => {
      this.id = params['id'];
    });
  }
  ngOnInit() {
    this.authorConnectionServer.getAuthorById(this.id).subscribe((data: Array<IAuthorModel>) => {
      this.AuthorData = data;
    });
    this.authorConnectionServer.getBooksByAuthorId(this.id).subscribe((data: Array<IBookModel>) => {
      this.booksData = data;
    });
    this.authorConnectionServer.getMagazinesByAuthorId(this.id).subscribe((data: Array<IMagazineModel>) => {
      this.magazinesData = data;
    });
  }
}
