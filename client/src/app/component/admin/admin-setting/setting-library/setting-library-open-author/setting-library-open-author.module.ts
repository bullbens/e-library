import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, Router, RouterModule } from '@angular/router';
import { SettingLibraryOpenAuthorComponent } from './setting-library-open-author.component';

const settingLibraryOpenAuthorRoutes: Routes = [
  {
    path: '', 
    component: SettingLibraryOpenAuthorComponent,
  }
]

@NgModule({
  declarations: [SettingLibraryOpenAuthorModule],
  imports: [
    CommonModule,
    RouterModule.forChild(settingLibraryOpenAuthorRoutes)
  ]
})
export class SettingLibraryOpenAuthorModule { }
