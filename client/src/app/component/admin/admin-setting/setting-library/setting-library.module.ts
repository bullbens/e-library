import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SettingLibraryComponent } from './setting-library.component';
import { RouterModule, Routes } from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';
import { PopupAdminAddBookComponent } from 'src/app/component/popup/admin/book/popup-admin-add-book/popup-admin-add-book.component';
import { PopupAdminAddAuthorComponent } from 'src/app/component/popup/admin/author/popup-admin-add-author/popup-admin-add-author.component';
import { SettingLibraryOpenBookComponent } from './setting-library-open-book/setting-library-open-book.component';
import { PopupAdminAddMagazineComponent } from 'src/app/component/popup/admin/magazine/popup-admin-add-magazine/popup-admin-add-magazine.component';
import { SettingLibraryOpenBookMagazineComponent } from './setting-library-open-book-magazine/setting-library-open-book-magazine.component';
import { SettingLibraryOpenAuthorComponent } from './setting-library-open-author/setting-library-open-author.component';
import { WithoutAnAuthorsComponent } from './without-an-authors/without-an-authors.component';

const librarySettingRoutes: Routes = [
  {
    path: '',
    component: SettingLibraryComponent, 
      },
      {
        path: 'setting-library-open-book/:id', 
        component: SettingLibraryOpenBookComponent,
      },
      {
        path: 'open-magazine/:id',
        component: SettingLibraryOpenBookMagazineComponent,
      },      
      {
        path: 'open-author/:id',
        component: SettingLibraryOpenAuthorComponent,
      },
      {
        path: 'without-author',
        component: WithoutAnAuthorsComponent,
      }
    ]

@NgModule({

  declarations: [
    SettingLibraryComponent,
    PopupAdminAddBookComponent,
    PopupAdminAddAuthorComponent, 
    PopupAdminAddMagazineComponent,
    SettingLibraryOpenBookComponent,
    SettingLibraryOpenBookMagazineComponent,
    SettingLibraryOpenAuthorComponent,
    WithoutAnAuthorsComponent
  ],

  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    RouterModule.forChild(librarySettingRoutes)
  ]
})

export class SettingLibraryModule { }
