import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ConteinerAdminHeaderComponent } from 'src/app/conteiner/admin/conteiner-admin-header/conteiner-admin-header.component';
import { ConteinerAdminFooterComponent } from 'src/app/conteiner/admin/conteiner-admin-footer/conteiner-admin-footer.component';

const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'setting',
        loadChildren: './admin-setting/admin-setting.module#AdminSettingModule',
      },
    ]
  }
];

@NgModule({
  declarations: [AdminComponent, ConteinerAdminHeaderComponent, ConteinerAdminFooterComponent],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild(adminRoutes)
  ]
})
export class AdminModule { }
