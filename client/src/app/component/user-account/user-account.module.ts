import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAccountComponent } from './user-account.component';
import { Routes, RouterModule } from '@angular/router';

const userRoutes: Routes = [
  {
    path: '',
    component: UserAccountComponent,
    children: [
      {
        path: 'library',
        loadChildren: './library/library.module#LibraryModule',
      },
      {
        path: 'personal',
        loadChildren: './personal-area/personal-area.module#PersonalAreaModule',
      },
    ]
  }
]

@NgModule({
  declarations: [UserAccountComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(userRoutes),
  ]
})
export class UserAccountModule { }
