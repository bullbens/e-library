import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.scss']
})
export class UserAccountComponent implements OnInit {

  public loginUser = this.cookie.get('login');

  constructor(public router: Router, private cookie: CookieService, private authService: AuthService) { }

  ngOnInit() {
  }

  exitUsers() {
    localStorage.clear();
    this.cookie.deleteAll();
    this.router.navigate(['/login']);

  }
}
