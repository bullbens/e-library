import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonalAreaComponent } from './personal-area.component';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

const personalRoutes: Routes = [
  {path: '', component: PersonalAreaComponent},
];

@NgModule({
  declarations: [PersonalAreaComponent],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild(personalRoutes)
  ]
})
export class PersonalAreaModule { }
