import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {IUserModel } from '../../../../../../shared/models/user.model';
import { UserConnectionServer } from 'src/app/shared/services/user-connection-server.service';

@Component({
  selector: 'app-personal-area',
  templateUrl: './personal-area.component.html',
  styleUrls: ['./personal-area.component.scss']
})

export class PersonalAreaComponent implements OnInit {

  public userDataName: string;
  public userDataSurname: string;

  constructor(private userConnectionServer: UserConnectionServer) { }
  
  ngOnInit() {
    this.userConnectionServer.getProfileUser().subscribe((data: Array<IUserModel>) => {
      this.userDataName = data['name'];
      this.userDataSurname = data['surname'];
      console.log(data);
    });
  }

  public onSubmitSaveSetting(setting: NgForm) {
    this.userConnectionServer.getProfileUser().subscribe(() => {
    });
  }
}
