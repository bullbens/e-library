import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibraryComponent } from './library.component';
import { Routes, RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';

const libraryRoutes: Routes = [
  {path: '', component: LibraryComponent},
];

@NgModule({
  declarations: [LibraryComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(libraryRoutes)
  ]
})
export class LibraryModule { }
