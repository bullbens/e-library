import { Component, OnInit } from '@angular/core';
import { IBookModel } from '../../../../../../shared/models/book.model';
import { BookConnectionServer } from 'src/app/shared/services/book-connection-server.service';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit {
  public title: string;
  public author: string;
  public books: Array<IBookModel>;
  constructor(private bookConnectionServer: BookConnectionServer) {
  }
  ngOnInit() {
    // this.bookConnectionServer.getBook().subscribe((data: Array<IBookModel>) => {
    //   this.books = data;
    //   console.log(this.books);
    // });
  }
}
