import { Component, OnInit, Output, EventEmitter, HostListener, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminConnectionServer } from 'src/app/shared/services/admin-connection-server.service';

@Component({
  selector: 'app-popup-admin-add-user',
  templateUrl: './popup-admin-add-user.component.html',
  styleUrls: ['./popup-admin-add-user.component.scss']
})
export class PopupAdminAddUserComponent implements OnInit {

  public addNewUserForm: FormGroup;
  public submitted: boolean = false;
  public checkValidators: boolean;
  @Output() public addNewUserResult = new EventEmitter();
  @Input() user: string;

  constructor(private adminConnectionServer: AdminConnectionServer, private formBuilder: FormBuilder) { }

  ngOnInit() {
    console.log(this.user);
    this.addNewUserForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      login: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      status: ['', Validators.required]
    });
  }

  get errorInput(): any {
    return this.addNewUserForm.controls;
  }

  public onSubmitAddNewUser(): void {
    this.submitted = true;
    if (this.addNewUserForm.invalid) {
      this.checkValidators = false;
      return;
    }
    if (this.addNewUserForm.valid) {
      this.adminConnectionServer.addUser(this.addNewUserForm.value).subscribe(data => {
        this.addNewUserResult.emit(false);
        console.log(data);
      });
      this.checkValidators = true;
    }
  }
}
