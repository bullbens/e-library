import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupAdminAddUserComponent } from './popup-admin-add-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [PopupAdminAddUserComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ]
})
export class PopupAdminAddUserModule { }
