import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IUserModel } from '../../../../../../../../shared/models/user.model';
import { AdminConnectionServer } from 'src/app/shared/services/admin-connection-server.service';

@Component({
  selector: 'app-popup-admin-update-user',
  templateUrl: './popup-admin-update-user.component.html',
  styleUrls: ['./popup-admin-update-user.component.scss']
})
export class PopupAdminUpdateUserComponent implements OnInit {

  public updateUserForm: FormGroup;
  public submitted: boolean = false;
  public checkValidators: boolean;
  @Output() public updateUserResult = new EventEmitter();
  @Input() user: Array<IUserModel>;

  constructor(private adminConnectionServer: AdminConnectionServer, private formBuilder: FormBuilder) { }

  ngOnInit() {
    console.log(this.user);
    this.updateUserForm = this.formBuilder.group({
      name: [this.user['name'], Validators.required],
      surname: [this.user['surname'], Validators.required],
      login: [this.user['login'], Validators.required],
      email: [this.user['email'], [Validators.required, Validators.email]],
      password: [this.user['password'], [Validators.required, Validators.minLength(6)]],
      status: [this.user['status'], Validators.required]
    });
  }

  get errorInput(): any {
    return this.updateUserForm.controls;
  }

  public onSubmitUpdateUser(): void {
    this.submitted = true;
    if (this.updateUserForm.invalid) {
      this.checkValidators = false;
      return;
    }
    if (this.updateUserForm.valid) {
      console.log(this.updateUserForm.value);
      this.adminConnectionServer.updateUserById(this.updateUserForm.value , this.user['_id']).subscribe(data => {
      });
      this.checkValidators = true;
      this.updateUserResult.emit(false);
    }
  }
}
  