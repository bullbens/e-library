import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupAdminUpdateUserComponent } from './popup-admin-update-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PopupAdminUpdateUserComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ]
})
export class PopupAdminUpdateUserModule { }
