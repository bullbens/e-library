import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthorConnectionServer } from 'src/app/shared/services/author-connection-server.service';
import { IAuthorModel } from '../../../../../../../../shared/models';

@Component({
  selector: 'app-popup-admin-add-author',
  templateUrl: './popup-admin-add-author.component.html',
  styleUrls: ['./popup-admin-add-author.component.scss']
})
export class PopupAdminAddAuthorComponent implements OnInit {
  
  @Output() public popupAuthorResult = new EventEmitter();
  @Input() public getAuthorData: Array<IAuthorModel>;
  @Input() public popupNameAuthor: string;
  
  public AuthorForm: FormGroup;
  public submitted: boolean = false;
  public checkValidators: boolean;

  constructor(private authorConnectionServer: AuthorConnectionServer, private formBuilder: FormBuilder) { }

  ngOnInit() {
    if (this.popupNameAuthor === "Add Author") {
      this.AuthorForm = this.formBuilder.group({
        name: ['', Validators.required]
      });
    }
    if (this.popupNameAuthor === "Remove Author") {
      this.AuthorForm = this.formBuilder.group({
        name: [this.getAuthorData['name'], Validators.required]
      });
    }
  }

  get errorInput() {
    return this.AuthorForm.controls;
  }

  public onSubmitAuthor() {
    if (this.popupNameAuthor === "Add Author") {
      this.submitted = true;
      if (this.AuthorForm.invalid) {
        this.checkValidators = false;
        return;
      }
      if (this.AuthorForm.valid) {
        this.authorConnectionServer.addAuthor(this.AuthorForm.value).subscribe(data => {
          this.popupAuthorResult.emit(false);
          console.log(data);
        });
        this.checkValidators = true;
      }
    }
    if (this.popupNameAuthor === "Remove Author") {
      this.submitted = true;
      if (this.AuthorForm.invalid) {
        this.checkValidators = false;
        return;
      }
      if (this.AuthorForm.valid) {
        this.authorConnectionServer.updateAuthorById(this.AuthorForm.value, this.getAuthorData['_id']).subscribe(data => {
          this.popupAuthorResult.emit(false);
          console.log(data);
        });
        this.checkValidators = true;
      }
    }

  }
}
