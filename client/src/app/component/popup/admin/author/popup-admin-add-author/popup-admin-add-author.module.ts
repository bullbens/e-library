import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupAdminAddAuthorComponent } from './popup-admin-add-author.component';

@NgModule({
  declarations: [PopupAdminAddAuthorComponent],
  imports: [
    CommonModule
  ]
})
export class PopupAdminAddAuthorModule { }
