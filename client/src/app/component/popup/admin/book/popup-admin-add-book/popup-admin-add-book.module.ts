import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupAdminAddBookComponent } from './popup-admin-add-book.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PopupAdminAddBookComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class PopupAdminAddBookModule { }
