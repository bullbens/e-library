import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IMagazineModel } from '../../../../../../../../shared/models/magazine.model';
import { BookConnectionServer } from 'src/app/shared/services/book-connection-server.service';
import { MagazineConnectionServer } from 'src/app/shared/services/magazine-connection-server.service';
import { IAuthorModel } from '../../../../../../../../shared/models/author.model';
import { IBookModel } from '../../../../../../../../shared/models/book.model';

@Component({
  selector: 'popup-admin-book',
  templateUrl: './popup-admin-add-book.component.html',
  styleUrls: ['./popup-admin-add-book.component.scss']
})
export class PopupAdminAddBookComponent implements OnInit {
  @Input() public allAuthors: Array<IAuthorModel>;
  @Input() public getBookData: IBookModel;
  @Input() public popupNameBook: string;
  @Output() public popupBookResult = new EventEmitter();

  public addNewBookForm: FormGroup;

  public submitted: boolean = false;
  public checkValidators: boolean;
  public selectedFile: File = null;
  public addNewAuthorBlock: boolean = false;
  public allMagazine: Array<IMagazineModel>;
  public nameAuthor: string;
  public authorsArr = [];
  public invisible: boolean = true;
  public addedAuthorError: string;
  public imgBase: string;

  constructor(
    private bookConnectionServer: BookConnectionServer,
    private magazineConnectionServer: MagazineConnectionServer,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.loadForm();
  }

  public getAllMagazine() {
    this.magazineConnectionServer.getMagazine().subscribe((data: Array<IMagazineModel>) => {
      this.allMagazine = data;
    });
  }

  public expression(event) {
    console.log(this.authorsArr);
    let param = this.authorsArr.find(item => {
      this.addedAuthorError = "Author already added";
      return item._id === event.target.value;
    })
    if (param === undefined) {
      this.allAuthors.map(author => {
        if(author._id === event.target.value) {
          this.authorsArr.push(author);
        }
      })
      this.addedAuthorError = "";
      // let i = event.target.value;
      // this.authorsArr.push(event.target.value);
      return
    }
  }
  public chooseImg(event): void {
    const file = event.target.files[0];
    const saveImg = (img: string) => {
      this.imgBase = img;
    };
    const reader = new FileReader();
    reader.onloadend = function () {
      saveImg(reader.result.toString());
    };
    reader.readAsDataURL(file);
  }

  public deleteAddedAuthor(event) {
    this.addedAuthorError = "";
    let indexAuthor = event.target.value;
    this.authorsArr.splice(indexAuthor, 1);
  }

  public loadForm() {
    if (this.popupNameBook === "Add Book") {
      this.getAllMagazine();
      this.addNewBookForm = this.formBuilder.group({
        title: ['', Validators.required],
        picture: [this.selectedFile, Validators.required],
        description: ['', Validators.required],
        text: ['', Validators.required],
        magazineId: [''],
        authorId: [this.authorsArr]
      });
    }
    if (this.popupNameBook === "Remove Book") {
      this.getAllMagazine();
      this.getBookData.authorId.map(item => {
        this.authorsArr.push(item);
      })
      this.addNewBookForm = this.formBuilder.group({
        title: [this.getBookData['title'], Validators.required],
        picture: ['', Validators.required],
        description: [this.getBookData['description'], Validators.required],
        text: [this.getBookData['text'], Validators.required],
        magazineId: [this.getBookData['magazineId']],
        authorId: [this.authorsArr]
      });
    }
  }

  get errorInput() {
    return this.addNewBookForm.controls;
  }

  public onSubmitAddNewBook() {
    this.submitted = true;
    if (this.addNewBookForm.invalid) {
      this.checkValidators = false;
      return;
    }
    if (this.popupNameBook === "Add Book") {
      if (this.addNewBookForm.valid) {
        const book = {
          _id: "some",
          picture: this.imgBase,
          title: this.addNewBookForm.value.title,
          text: this.addNewBookForm.value.text,
          description: this.addNewBookForm.value.description,
          magazineId: this.addNewBookForm.value.magazineId,
          authorId: this.authorsArr,
        }
        this.bookConnectionServer.addBook(book).subscribe(data => {
          this.popupBookResult.emit(false);
        });
        this.checkValidators = true;
      }
    }
    if (this.popupNameBook === "Remove Book") {
      if (this.addNewBookForm.valid) {
        if (this.addNewBookForm.valid) {
          const book = {
            _id: this.getBookData['_id'],
            picture: this.imgBase,
            title: this.addNewBookForm.value.title,
            text: this.addNewBookForm.value.text,
            description: this.addNewBookForm.value.description,
            magazineId: this.addNewBookForm.value.magazineId,
            authorId: this.authorsArr,
          }
          this.bookConnectionServer.updateBookById(book, this.getBookData['_id']).subscribe(data => {
            this.popupBookResult.emit(false);
          });
          this.checkValidators = true;
        }
      }
    }
  }
}

