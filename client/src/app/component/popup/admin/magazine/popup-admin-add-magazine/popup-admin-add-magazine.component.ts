import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MagazineConnectionServer } from 'src/app/shared/services/magazine-connection-server.service';
import { IAuthorModel } from '../../../../../../../../shared/models';

@Component({
  selector: 'popup-admin-magazine',
  templateUrl: './popup-admin-add-magazine.component.html',
  styleUrls: ['./popup-admin-add-magazine.component.scss']
})
export class PopupAdminAddMagazineComponent implements OnInit {

  @Input() public allAuthors: Array<IAuthorModel>;
  @Input() public popupNameMagazine: string;
  @Input() public getMagazineData: string;
  @Output() public popupMagazineResult = new EventEmitter();
  public addNewMagazineForm: FormGroup;
  public submitted: boolean = false;
  public checkValidators: boolean;

  constructor(private magazineConnectionServer: MagazineConnectionServer, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.formMagazine();
  }
  public formMagazine(): void {
    if (this.popupNameMagazine === "Add Magazine") {
      this.addNewMagazineForm = this.formBuilder.group({
        title: ['', Validators.required],
        picture: ['', Validators.required],
        description: ['', Validators.required],
        text: ['', Validators.required],
        publisher: ['', Validators.required],
        authorId: [Validators.required]
      });
    }
    if (this.popupNameMagazine === "Remove Magazine") {
      this.addNewMagazineForm = this.formBuilder.group({
        title: [this.getMagazineData['title'], Validators.required],
        picture: [this.getMagazineData['picture'], Validators.required],
        description: [this.getMagazineData['description'], Validators.required],
        text: [this.getMagazineData['text'], Validators.required],
        publisher: [this.getMagazineData['publisher'], Validators.required],
        authorId: ['', Validators.required]
      });
    }
  }

  get errorInput(): any {
    return this.addNewMagazineForm.controls;
  }

  public onSubmitAddNewMagazine(): void {
    this.submitted = true;
    if (this.addNewMagazineForm.invalid) {
      this.checkValidators = false;
      return;
    }
    if (this.addNewMagazineForm.valid) {
      if (this.popupNameMagazine === "Add Magazine") {
        this.magazineConnectionServer.addMagazine(this.addNewMagazineForm.value).subscribe(data => {
          this.popupMagazineResult.emit(false);
        });
        this.checkValidators = true;
      }
      if (this.popupNameMagazine === "Remove Magazine") {
        this.magazineConnectionServer.updateMagazineById(this.addNewMagazineForm.value, this.getMagazineData['_id']).subscribe(data => {
          this.popupMagazineResult.emit(false);
        });
      }
    }
  }
}
