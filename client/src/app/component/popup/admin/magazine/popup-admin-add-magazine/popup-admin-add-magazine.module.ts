import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PopupAdminAddMagazineModule],
  imports: [
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PopupAdminAddMagazineModule { }
