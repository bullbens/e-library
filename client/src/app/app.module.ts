import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { AuthGuard } from './shared/guards/auth.guard';
import { AuthService } from './shared/services/auth.service';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';
import { AuthUserGuard } from './shared/guards/auth-user.guard';
import { CookieService } from 'ngx-cookie-service';
import { StartPageGuard } from './shared/guards/start-page.guard';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [StartPageGuard],
    children: [
      { path: 'login', loadChildren: '../app/component/home/login/login.module#LoginModule' },
      { path: 'check', loadChildren: '../app/component/home/reg/reg.module#RegModule' },
    ]
  },

  {
    path: 'admin',
    loadChildren: './component/admin/admin.module#AdminModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'user',
    loadChildren: './component/user-account/user-account.module#UserAccountModule',
    canActivate: [AuthUserGuard],
  },

  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [AuthService, AuthGuard, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
