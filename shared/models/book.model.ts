import { IAuthorModel } from ".";

export interface IBookModel {
    _id: any;
    picture: string;
    title: string;
    text: string;
    description: string;
    magazineId: string;
    authorId: any;
}
