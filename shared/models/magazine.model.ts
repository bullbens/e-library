export interface IMagazineModel {
    _id: any;
    picture: string;
    title: string;
    text: string;
    description: string;
    authorId: string;
    publisher: string;
}
