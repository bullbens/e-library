
export interface IUserModel {
    _id:any;
    name: string,
    surname: string,
    email: string,
    login: string,
    password: string,
    status: string,
    closed: boolean,
    role: UserRole
}
export enum UserRole{
    user=1,
    admin=2
}
