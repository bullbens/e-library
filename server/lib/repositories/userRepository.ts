import * as mongoose from 'mongoose';
import { IUserModel } from '../../../shared/models/user.model';

const Schema = mongoose.Schema;
interface IUserEntity extends IUserModel, mongoose.Document { }
export const UserSchema = new Schema({
  name: String,
  surname: String,
  email: String,
  login: String,
  password:String,
  status: String,
  closed: Boolean,
  role: Number
}
);
export const UserRepository = mongoose.model<IUserEntity>('user', UserSchema);

