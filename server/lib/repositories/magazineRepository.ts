import * as mongoose from 'mongoose';
import { IMagazineModel } from '../../../shared/models/magazine.model';

const Schema = mongoose.Schema;
interface IMagazineEntity extends IMagazineModel, mongoose.Document { }
export const MagazineSchema = new Schema({
    title: String,
    picture: String,
    description: String,
    text: String,
    authorId: String,
    publisher: String,
}
);
export const MagazineRepository = mongoose.model<IMagazineEntity>('magazine', MagazineSchema);