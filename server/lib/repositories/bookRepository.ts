import * as mongoose from 'mongoose';
import { IBookModel } from '../../../shared/models/book.model';


let ObjectId = mongoose.Schema.Types.ObjectId;

interface IBookEntity extends IBookModel, mongoose.Document { }
export const BookSchema = new mongoose.Schema({
    picture: String,
    title: String,
    text: String,
    description: String,
    magazineId: String,
    authorId: [ObjectId]
});
export const BookRepository = mongoose.model<IBookEntity>('book', BookSchema);