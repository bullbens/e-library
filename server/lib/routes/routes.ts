import { Request, Response, NextFunction } from "express";
import { BookController } from "../controllers/bookController";
import { AuthorController } from "../controllers/authorController";
import { AuthMiddleware } from "../middlewares/authMiddleware";
import { AuthController } from "../controllers/authController";
import { UserRole } from "../../../shared/models/user.model";
import {AdminController} from "../controllers/adminController";
import {UserController} from "../controllers/userController";
import {MagazineController} from "../controllers/magazineController";

export class Routes {

    public bookController: BookController = new BookController()
    public authorController: AuthorController = new AuthorController()
    public authController: AuthController = new AuthController()
    public adminController: AdminController = new AdminController();
    public userController: UserController = new UserController();
    public magazineController: MagazineController = new MagazineController();

    public routes(app): void {

        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'GET request successfulll!!!!'
                })
            })
        // Auth
        app.route('/auth/profile')
            .get(AuthMiddleware([UserRole.user,UserRole.admin]),this.authController.profile);
        app.route('/auth/register')
            .post(this.authController.register);
        app.route('/auth/login')
            .post(this.authController.login);

        //Admin
        app.route('/admin/user/:_id')
            .get(AuthMiddleware([UserRole.admin]), this.adminController.getUserById)
            .delete(AuthMiddleware([UserRole.admin]), this.adminController.deleteUserById);
        app.route('/admin/users')
            .get(AuthMiddleware([UserRole.admin]), this.adminController.getUsers);   
        app.route('/admin/closed')
            .get(AuthMiddleware([UserRole.admin]), this.adminController.getClosedUsers);
        
        //Users
        app.route('/user/role')
            .get(AuthMiddleware([UserRole.admin, UserRole.user]),this.userController.getUserRole);
        app.route('/user/update/:_id')
            .put(AuthMiddleware([UserRole.admin, UserRole.user]), this.userController.updateUserById);

        // Book
        app.route('/book/')
            .get(AuthMiddleware([UserRole.admin, UserRole.user]), this.bookController.getBook)
            .post(this.bookController.addBook);
        app.route('/book/:bookId')
            .get(AuthMiddleware([UserRole.admin]), this.bookController.getBookById)
            .put(AuthMiddleware([UserRole.admin]), this.bookController.updateBookById)
            .delete(AuthMiddleware([UserRole.admin]), this.bookController.deleteBookById);
        app.route('/books/without-author/')
            .get(AuthMiddleware([UserRole.admin]),this.bookController.getBookWithoutAuthor);
        app.route('/book/search/:_title')
            .get(AuthMiddleware([UserRole.admin, UserRole.user]),this.bookController.postBooksBySearch);
        // Magazine
        app.route('/magazine/')
            .get(AuthMiddleware([UserRole.admin, UserRole.user]), this.magazineController.getMagazine)
            .post(AuthMiddleware([UserRole.admin]), this.magazineController.addMagazine);
         app.route('/magazine/:_id')
            .get(AuthMiddleware([UserRole.admin]), this.magazineController.getMagazineById)
            .put(AuthMiddleware([UserRole.admin]), this.magazineController.updateMagazineById)
            .delete(AuthMiddleware([UserRole.admin]), this.magazineController.deleteMagazineById);


        // Author 
        app.route('/author')
            .get(AuthMiddleware([UserRole.admin]), this.authorController.getAuthor)
            .post(AuthMiddleware([UserRole.admin]), this.authorController.addAuthor);
        app.route('/author/:_id')
            .get(AuthMiddleware([UserRole.admin]), this.authorController.getAuthorById)
            .put(AuthMiddleware([UserRole.admin]), this.authorController.updateAuthorById)
            .delete(AuthMiddleware([UserRole.admin]), this.authorController.deleteAuthorById);
        app.route('/author/book/:_id')
            .get(AuthMiddleware([UserRole.admin]), this.authorController.getBookByAuthorId);
        app.route('/author/amgazine/:_id')
            .get(AuthMiddleware([UserRole.admin]), this.authorController.getMagazineByAuthorId);

    }
}