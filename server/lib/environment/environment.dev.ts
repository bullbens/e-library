import { IEnvironment } from "./environment";

export const developmentEnvironment:IEnvironment={
    mongoDbConnectionString:"mongodb://bullbens:vgn-sz2hrp@library-shard-00-00-dqjml.gcp.mongodb.net:27017,library-shard-00-01-dqjml.gcp.mongodb.net:27017,library-shard-00-02-dqjml.gcp.mongodb.net:27017/test?ssl=true&replicaSet=Library-shard-0&authSource=admin&retryWrites=true",
    port: 3000
}