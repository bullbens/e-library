import * as jsonwebtoken from "jsonwebtoken";
import { authConfig } from "../config"
import { Request, Response, NextFunction } from 'express';
import { IUserModel , UserRole} from "./../../../shared/models/user.model";


export interface RequestModel<Params> extends Request {
  user: IUserModel;
  params: Params;
}
export interface RequestPost<BodyType> extends Request {
  body: BodyType;
}

export const AuthMiddleware = (roles:UserRole[]) => {
  return (req: RequestModel<{}>, res: Response, next: NextFunction) => {
    var token: any = req.headers['x-access-token'];
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
  
    jsonwebtoken.verify(token, authConfig.secret, function (err, decoded) {
      if (err) {
        return res.status(401).send({ auth: false, message: 'Failed to authenticate token.' });
      }
      req.user = decoded;
      var isRoleExist = roles.find(item => item == req.user.role);

      if(!isRoleExist){
        return res.status(403).send({ auth: false, message: 'Access denied!' +  isRoleExist});
      }
      next();
    });
  };
}