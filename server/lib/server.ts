import app from './app';
import * as https from 'https';
import * as fs from 'fs';
import { environment } from "./environment/environment";


const httpsOptions = {
    key: fs.readFileSync('./config/key.pem'),
    cert: fs.readFileSync('./config/cert.pem')
}

https.createServer(httpsOptions, app).listen(environment().port, () => {
    console.log('Express server listening on port ' + environment().port);
})