import { Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { BookRepository } from '../repositories/bookRepository';
import { AuthorRepository } from '../repositories/authorRepository';
import { mongo } from 'mongoose';

export class BookController {

    public getBook(req: RequestModel<{}>, res: Response) {
        BookRepository.aggregate([
            {
                $lookup: {
                    from: "authors",
                    localField: "authorId",
                    foreignField: "_id",
                    as: "authorId",
                }
            }
        ]).exec((err, resulting) => {
            if (err) return res.send('Error');
            console.log(resulting);
            res.json(resulting);
        })
    }
    getBookWithoutAuthor(req: RequestModel<{}>, res: Response) {
        BookRepository.find({ authorId: [] }, (err, book) => {
            if (err) return res.status(500).send('Error on the sssserver.');
            res.json(book);
        })
    }

    public getBookById(req: RequestModel<{ bookId: number }>, res: Response) {
        BookRepository.findById(req.params.bookId, (err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }
    public addBook(req: RequestModel<{}>, res: Response) {
        let newBook = new BookRepository({
            picture: req.body.picture,
            title: req.body.title,
            text: req.body.text,
            description: req.body.description,
            magazineId: req.body.magazineId,
            authorId: req.body.authorId
        });
        newBook.save((err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }
    public postBooksBySearch(req: RequestModel<{ _title: string }>, res: Response) {
        BookRepository.aggregate([
            {
                $lookup: {
                    from: "book",
                    localField: {$regex:  /_title/},
                    foreignField: "title",
                    as: "book",
                }
            }
        ]).exec((err, resulting) => {
            if (err) return res.send('Error');
            console.log(resulting);
            res.json(resulting);
        })
    }
    // BookRepository.find({ title: { $regex: req.params._title, $options: 'i' }, authorId: { $not: { $size: 0 } }}, (err, books) => {
    //         if (req.params._title === '') {
    //             res.json("")
    //         }
    //         if (err) return res.status(500).send("Error on the server");
    //         res.json(books)
    //     })
    // }

    public updateBookById(req: RequestModel<{ bookId: any }>, res: Response) {
        BookRepository.findByIdAndUpdate({ _id: req.params.bookId }, req.body, { new: true }, (err, contact) => {
            if (err) return res.status(500).send(req.body.bookId);
            res.json();
        });
    }

    public deleteBookById(req: RequestModel<{ bookId: number }>, res: Response) {
        BookRepository.remove({ _id: req.params.bookId }, (err) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json({ message: 'Successfully deleted contact!' });
        });
    }

}
        //     var books = new Array();
        //     book.forEach(item => {
        //         for (let i = 0; i < item.authorId.length; i++) {
        //             let index = item.authorId[i];
        //             AuthorRepository.findById({ _id: index }, (err, author) => {
        //                 if (err) res.send('Error');
        //                 book.map(item => item.authorId[0] = author['name']);
        //                 books[i] = book;

        //             })
        //         }
        //     })
        //     console.log(books);
        // })
