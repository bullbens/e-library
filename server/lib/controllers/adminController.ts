import { RequestModel } from '../middlewares/authMiddleware';
import { Response } from 'express';
import { UserRepository } from '../repositories/userRepository';

export class AdminController {
    public getUserById(req: RequestModel<{ _id: string }>, res: Response) {
        UserRepository.findById({ _id: req.params._id }, (err, user) => {
            if (err) return res.status(500).send('Error on the server');
            res.json(user);
        });
    }
    public getUsers(req: RequestModel<{}>, res: Response) {
        UserRepository.find({}, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
    }
    public getClosedUsers(req: RequestModel<{ closed: boolean }>, res: Response) {
        UserRepository.find({ closed: true }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.')
            res.json(user);
        })
    }
    public deleteUserById(req: RequestModel<{ _id: string }>, res: Response) {
        UserRepository.remove({ _id: req.params._id }, (err) => {
            if (err) return res.status(500).send('Error on the server');
            res.json({ message: 'Ok Delete User!' });
        });
    }
}