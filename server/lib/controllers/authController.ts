
import { RequestModel, RequestPost } from '../middlewares/authMiddleware';
import { Response, Router } from 'express';
import * as bcryptjs from "bcryptjs";
import * as jsonwebtoken from "jsonwebtoken";
import { authConfig } from "../config"
import { UserRepository } from '../repositories/userRepository';
import { IUserModel, UserRole } from '../../../shared/models';


export class AuthController {

    public register(req: RequestPost<IUserModel>, res: Response) {
        var hashedPassword = bcryptjs.hashSync(req.body.password, 8);

        UserRepository.create({
            name: req.body.name,
            surname: req.body.surname,
            email: req.body.email,
            login: req.body.login,
            password: hashedPassword,
            status: req.body.status,
            closed: req.body.closed,
            role: UserRole.user
        },
            (err, user) => {
                if (err) return res.status(500).send("There was a problem registering the user.")
                // create a token
                var token = jsonwebtoken.sign({ id: user._id, name: user.name, surname: user.surname, email: user.email, login: user.login, status: user.status, closed: user.closed, role: UserRole.user}, authConfig.secret, {
                    expiresIn: 86400 // expires in 24 hours
                    
                });
                res.status(200).send({ auth: true, token: token, user: user });
            });
    }

    public login(req: RequestPost<{ login: string, password: string }>, res: Response) {
        UserRepository.findOne({ login: req.body.login }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            if (!user) return res.status(400).send('No user found.');
            var passwordIsValid = bcryptjs.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });
            var token = jsonwebtoken.sign({ id: user._id, name: user.name, surname: user.surname, email: user.email, login: user.login, status: user.status, closed: user.closed, role: user.role}, authConfig.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            res.status(200).send({ auth: true, token: token, user: user});
        });
    }

    public profile(req: RequestModel<{}>, res: Response) {
        res.status(200).send(req.user);
    }
}
 