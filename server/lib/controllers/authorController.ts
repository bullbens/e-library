import { Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { AuthorRepository } from '../repositories/authorRepository';
import { BookRepository } from '../repositories/bookRepository';
import { MagazineRepository } from '../repositories/magazineRepository';

export class AuthorController {


    public getAuthor(req: RequestModel<{}>, res: Response) {
        AuthorRepository.find({}, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    } 

    public getAuthorById(req: RequestModel<{ _id: number }>, res: Response) {
        AuthorRepository.findById(req.params._id, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public getBookByAuthorId(req: RequestModel<{ _id: string }>, res: Response) {
        BookRepository.find({ authorId: req.params._id }, (err, books) => {
            if (err) return res.status(500).send('Error get author books.');
            res.json(books);
        })
    }
    public getMagazineByAuthorId(req: RequestModel<{ _id: string }>, res: Response) {
        MagazineRepository.find({ authorId: req.params._id }, (err, magazine) => {
            if (err) return res.status(500).send('Error get author books.');
            res.json(magazine);
        })
    }
    public addAuthor(req: RequestModel<{}>, res: Response) {
        let newAuthor = new AuthorRepository(req.body);
        newAuthor.save((err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public updateAuthorById(req: RequestModel<{ _id: any }>, res: Response) {
        AuthorRepository.findByIdAndUpdate({ _id: req.params._id }, req.body, { new: true }, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
            BookRepository.findOneAndUpdate({ _id: author._id }, { authorName: author.name }, { new: true }, (err, book) => {
                if (err) return res.send('error');
                console.log(book)
            })
        });
    }

    public deleteAuthorById(req: RequestModel<{ _id: number }>, res: Response) {
        BookRepository.find({}, (err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            book.forEach(item => {
                for (let i = 0; i < item.authorId.length; i++) {
                    if (item.authorId[i]['_id'] === req.params._id) {
                        let authorBook = item.authorId[i];
                        BookRepository.update({_id: item._id},{$pull: {'authorId': authorBook}} ,(err, bo) => {
                            if (err) return res.status(500).send('Error on the server.');
                        })
                    }
                }
            })
        })
        AuthorRepository.deleteOne({ _id: req.params._id }, (err) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json({ message: 'Successfully deleted contact!' });
        });


    }
}