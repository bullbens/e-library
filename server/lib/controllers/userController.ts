import { RequestModel } from '../middlewares/authMiddleware';
import { Response } from 'express';
import * as bcryptjs from "bcryptjs";
import { UserRepository } from '../repositories/userRepository';

export class UserController {

    public getUserRole(req: RequestModel<{}>, res: Response) {
        req.user;
        return res.send({role: req.user.role, closed: req.user.closed});
    }

    public updateUserById(req: RequestModel<{_id: any}>, res: Response) {
        var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
        req.body.password = hashedPassword;
         UserRepository.findByIdAndUpdate({_id: req.params._id}, req.body , {new: true}, (err, user) => {
             if(err) return res.status(500).send('Error on the server.')
             res.json(user);
         });
     }

}