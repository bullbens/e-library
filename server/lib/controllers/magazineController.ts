import { Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { MagazineRepository } from '../repositories/magazineRepository';

export class MagazineController {


    public getMagazine(req: RequestModel<{}>, res: Response) {
        MagazineRepository.find({}, (err, magazine) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(magazine);
        });
    }
    public getMagazineById(req: RequestModel<{ _id: string }>, res: Response) {
        MagazineRepository.findById(req.params._id, (err, magazine) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(magazine);
        });
    }

    public addMagazine(req: RequestModel<{}>, res: Response) {
        let newMagazine = new MagazineRepository(req.body);
        newMagazine.save((err, magazine) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(magazine);
        });
    }

    public updateMagazineById(req: RequestModel<{ _id: any }>, res: Response) {
        MagazineRepository.findByIdAndUpdate({_id:req.params._id }, req.body, { new: true }, (err, magazine) => {
            if (err) return res.status(500).send(req.body._id);
            res.json();
        });
    }

    public deleteMagazineById(req: RequestModel<{ _id: any }>, res: Response) {
        MagazineRepository.remove({ _id: req.params._id }, (err) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json({ message: 'Successfully deleted contact!' });
        });
    }

}